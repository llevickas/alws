### How-To

1. download **alws** to somewhere comfortable reaching again and again.
2. Rename `/.env.dist` to `/.env` and edit it, to your liking.
	- Most importantly, set `SITES_HOME` variable to where you are storing your websites,
	- e.g.: `/home/myusername/Documents/sites`
	- Make sure the folder exists and writable, before running this with `docker-compose`.
3. Rename `/httpd/apache.conf.dist` to `/httpd/apache.conf`.
	- Every website will be a `<VirtualHost ...>`
	- Edit every line that has `CHANGE_ME` value in it
	- E.g.: `DocumentRoot "${SITES_HOME}/my-local-wordpress"`
	- E.g.: `ServerName mylocalwp.${HTTPD_SERVER_NAME}`
4. Start the this compose with `sudo docker-compose up`
5. Access the websites as you have entered them in the `/httpd/apache.conf` file.
	- E.g.: `http://mylocalwp.localhost/`

*The command will download and start the servises, you you do not have them already.
After building is done, the stack should come up shortly, ready to serve. Be sure the ports specified in `/docker-compose.yml` are free, and all volumes existing and readable.*
